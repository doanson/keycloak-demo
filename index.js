const express = require("express");
const PORT = process.env.PORT || 3000;
const Keycloak = require("keycloak-connect");
const session = require("express-session");
const handlebars = require("express-handlebars");

const memoryStore = new session.MemoryStore();
const keycloak = new Keycloak({
  store: memoryStore,
});
const app = express();

app.use(express.static("public"));
app.set("view engine", "handlebars");

app.engine(
  "handlebars",
  handlebars({
    defaultLayout: "main",
  })
);

app.use(
  session({
    secret: "mySecret",
    resave: false,
    saveUninitialized: true,
    store: memoryStore,
  })
);

app.use(
  keycloak.middleware({
    logout: "/logout",
    admin: "/",
  })
);


app.get("/", (req, res) => {
  res.render("home");
});

app.get("/login", keycloak.protect(), function (req, res) {
  res.render("home", { authenticated: true });
});
app.get("/admin/login", keycloak.protect(), function (req, res) {
  res.render("home", { authenticated: true, title: 'AMIN PAGE', role: 'admin' });
});

app.get("/profile", keycloak.protect(), (req, res) => {
  // res.render('profile');
  console.log('res: ', res);
  
  console.log('res.data: ', res.data);
  res.render("profile", { authenticated: true });
});

app.listen(PORT, () => {
  console.log(`App is listening on PORT ${PORT}`);
});
